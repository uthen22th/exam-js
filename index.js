const exData = require('./example-data.json')
//1. หาชื่อ product หลักที่มี id = 10207  ตอบเฉพาะชื่อ <name>
let findP = exData.find( product => product.id === 10207)
console.log('ข้อ 1 > ' , findP.name);

//2. รวมราคา (price) product หลักทั้งหมด ตอบเป็นตัวเลข
let totalPrice = 0
exData.forEach(data =>{
    totalPrice += data.price
})
console.log('ข้อ 2 > ' , totalPrice);

//3. หาว่าสินค้าหลักทั้งหมด vat_percent เป็น 0 หรือไม่ ตอบเป็น Boolean
let totalVat = exData.every(data => data.vat_percent === 0)
console.log('ข้อ 3 > ' , totalVat);

/*4. ให้หา product หลักทั้งหมด ที่มี products รอง ราคา (price) ต่ำกว่าเท่ากับ 200 
แล้วถ้าก้อนหลักไหนไม่มี products รองเหลืออยู่เลย (array เปล่า) ก็ไม่ให้เอา product หลักนั้นมาแสดง*/
let checkProduct = exData.filter(data => {
   return data.products.length > 0 && data.products.find(item => item.price<=200)
    
})
let newProduct = checkProduct.map(product => product.name)
console.log('ข้อ 4 > ' , newProduct);

//5. เพิ่ม key isToggle = false ให้กับ product หลักทุกตัว ตอบประมาณนี้ { … , isToggle: false }
let newData = exData.reduce((a,data) =>{
    if(data.id){
        let addKey = {id: data.id, isToggle: false}
        a.push(addKey)
    }
    return a
},[])
console.log('ข้อ 5 > ')
newData.forEach(data => console.log(data))

/*6. รวมราคา products รอง ทั้งหมดไว้ในตัวแปรใหม่ ชื่อ productsTotalPrice ตอบ เฉพาะชื่อ 
และ productsTotalPrice เช่น  { name: ‘my product’,  productsTotalPrice: 103000  }*/
let dataProductTotal = exData.reduce((total,data) => {
    let totalPrice = 0
    data.products.forEach(item => {return totalPrice+=item.price})
    if(data.name){
        let addKey = {name: data.name, productsTotalPrice: totalPrice}
        total.push(addKey)
    }
    return total
 },[])
 console.log('ข้อ 6 > ')
 dataProductTotal.forEach(data => console.log(data));

 /*7. หา product หลัก ที่มี is_editable_price = false หลังจากนั้นให้รวมน้ำหนักสินค้าย่อยทั้งหมด 
 ตอบเอาเฉพาะชื่อ และน่ำหนักรวม เช่น { name: ‘Wow product’, totalSubProductWeight: 200 }*/
 let editablePriceData = exData.filter(data => {
    return data.is_editable_price === false
 })
let editablePrice = editablePriceData.reduce((total,data)=>{
    let totalWeight = 0
    data.products.forEach(item => {return totalWeight+=item.weight})
    if(data.name){
        let addKey = {name: data.name, totalSubProductWeight: totalWeight}
        total.push(addKey)
    }
    return total
},[])
console.log('ข้อ 7 > ')
editablePrice.forEach(data => console.log(data))

//8. หาตำแหน่งแรก (index)ของ product หลัก ตัวแรก ที่มี products รอง ตัวแปร type ไม่ใช่ array ว่าง
let indexOfProduct = exData.findIndex(data => { return data.products.find(item => {return item.type != '' })})
console.log('ข้อ 8 > ', indexOfProduct);

/*9. ให้เอาเฉพาะก้อนหลัก ที่ products รอง มี type ทุกอัน เช่น ถ้า products: [{ type: ‘eiei’, name: a } , { name: b }] 
แสดงว่าก้อนนี้ไม่เอา เนื่องจาก name b ไม่มี type ทำให้ไม่เอา product หลักอันนี้ หลังจากนั้น ให้เอา type.id 
มาเก็บไว้ใน array ก้อนหนึ่งเพื่อเอามาตอบ คำตอบจะประมาณนี้ [ 120, 300, 500 ,600 , 902]*/
let newDataType = exData.filter(data => {
    return data.products.length > 0 && data.products.every(item => {return item.type!== undefined && item.type.length>0 })
 })
 let arr =[]
 newDataType.forEach(data => {
     return data.products.forEach(item =>{
         return item.type.forEach((t) => {
             return arr.push(t.id)
            })
        })
    }) 
 console.log('ข้อ 9 > ',arr);

//10.
// โจทย์ข้อสุดท้ายให้ใช้ promise ในการช่วยให้ console.log ออกมาเรียงลำดับ 1 , 2 , 3 ตามลำดับ
// โดยไม่อนุญาติให้แก้เลขเวลาใน setTimeout โดยให้ เรียก firstFunction ก่อน หลังจากนั้นจึงตามด้วย
// secondFunction และ thirdFunction ตามลำดับ
const firstFunction = () => {
    return new Promise((resolve)=>{
        setTimeout(() => {
            resolve()
            console.log('1')
        }, 3000)
    })
    
}

const secondFunction = () => {
    setTimeout(() => {
        console.log('2')
    }, 1000)
}

const thirdFunction = () => {
    setTimeout(() => {
        console.log('3')
    }, 2000)
}
console.log('ข้อ 10 > ');
firstFunction().then(()=>{secondFunction()}).then(()=> {thirdFunction()})



